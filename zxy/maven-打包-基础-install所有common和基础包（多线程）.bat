::当前目录
set work_path=%~dp0
cd /d %work_path%
cd ../../
set work_path=%cd%



@echo off
echo 'start maven install api'
set dir=%work_path%

echo %dir%\project\pom.xml

call mvn clean install -f %dir%\project\pom.xml -Dmaven.test.skip=true

echo ---------------------------------------------------
@REM 打包所有common
for /D %%i in (%dir%\*ommon-*) do (
	echo %%i
	start "%%i" cmd /k "call mvn clean install -f %%i\pom.xml  -Dmaven.test.skip=true"
	
)
echo ---------------------------------------------------
@REM 打包所有parent
for /D %%i in (%dir%\*parent) do (
	echo %%i
	start "%%i" cmd /k "call mvn clean install -f %%i\pom.xml"
)

echo 'end maven install api!'
::pause
