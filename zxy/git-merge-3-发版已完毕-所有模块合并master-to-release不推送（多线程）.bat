set checkoutRemoteBranch=release
echo Remote Branch Name=%checkoutRemoteBranch%

set work_path=%~dp0
cd /d %work_path%
cd ../../
set work_path=%cd%



echo ====Unsubmitted requests need to be processed before detection!!! Otherwise, it cannot be detected!!!====
pause


::检出分支
for /d %%s in (%work_path%\*) do (
cd %%s
echo Check out branches %% %%s %% %checkoutRemoteBranch%
git checkout %checkoutRemoteBranch%

)
::git checkout -b %checkoutRemoteBranch%  origin/%checkoutRemoteBranch%

::拉取代码
for /d %%s in (%work_path%\*) do (
cd %%s
echo Update Code %% %%s %% %checkoutRemoteBranch%
start "%%s" cmd /c "git pull origin %checkoutRemoteBranch%"
)

::延迟避免没更新代码就合并了，可以手动 取消延迟
TIMEOUT /T 60

::拉取合并
for /d %%s in (%work_path%\*) do (
cd %%s
echo Pull and merge master branches %% %%s %% %checkoutRemoteBranch%
start "%%s" cmd /c "git pull origin master"
)


::for /d %%s in (%work_path%\*) do (
::cd %%s
::echo 推送 %% %%s %% %checkoutRemoteBranch%
::git push origin release
::)


echo ok...
pause

