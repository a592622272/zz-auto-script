set /p tagName=Please enter the name of the tag you want to create:

echo The tag name entered=%tagName%

set work_path=%~dp0
cd /d %work_path%
cd ../
set work_path=%cd%




pause


for /d %%s in (%work_path%\*) do (
cd %%s

git tag -a %tagName% -m "new tag"


)

for /d %%s in (%work_path%\*) do (
cd %%s

start "%%s" cmd /c "git push origin --tags"

)


echo ok...
pause

