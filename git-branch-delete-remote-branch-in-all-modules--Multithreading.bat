set /p deleteRemoteBranch=Please enter the 'Remote Branch Name' to be deleted:
echo Remote branch name to delete::::%deleteRemoteBranch%

pause

set work_path=%~dp0
cd /d %work_path%
cd ../
set work_path=%cd%



for /d %%s in (%work_path%\*) do (
cd %%s
echo deleteBranch %% %%s %% %deleteRemoteBranch%
start "%%s" cmd /c "git push origin --delete %deleteRemoteBranch%"


)


echo ok...
pause