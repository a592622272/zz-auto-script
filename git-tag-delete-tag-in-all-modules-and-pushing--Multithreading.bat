set /p tagName=Please enter the tag name to delete:

echo The tag name entered=%tagName%

set work_path=%~dp0
cd /d %work_path%
cd ../
set work_path=%cd%




pause


for /d %%s in (%work_path%\*) do (
cd %%s

git tag -d %tagName%

start "%%s" cmd /c "git push origin :refs/tags/%tagName%"


)


echo ok...
pause

