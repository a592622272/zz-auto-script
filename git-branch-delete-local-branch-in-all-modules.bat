set /p deleteRemoteBranch=Please enter the 'Local Branch Name' to be deleted:
echo Local branch name to be deleted=%deleteRemoteBranch%

pause

set work_path=%~dp0
cd /d %work_path%
cd ../
set work_path=%cd%


echo ====Please check carefully!!! About to delete all local branches under the current directory, current directory= %% %work_path% %% delete local branch= %% %deleteRemoteBranch%====




for /d %%s in (%work_path%\*) do (
cd %%s
echo Delete Branch %% %%s %% %deleteRemoteBranch%
git branch -d %deleteRemoteBranch%

)


echo ok...
pause